# cgi

![](https://img.shields.io/badge/written%20in-Javascript%20%28node.js%29-blue)

A minimax AI for the card game Hearts.

It uses depth-first minimax to evaluate a round. It's not very smart.
Most of the underlying structure should be portable to other card games.


## Download

- [⬇️ sample_game.log](dist-archive/sample_game.log) *(1.84 KiB)*
- [⬇️ cgi.js](dist-archive/cgi.js) *(13.35 KiB)*
